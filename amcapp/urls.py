from django.conf.urls import include, url
from django.contrib import admin
from amcapp import forms
# from django.contrib.auth import views
from amcapp import views
# from amcapp.forms import LoginForm


urlpatterns = [

    # default url displays home page
    url(r'^$', views.home_index_page, name='home_index_page'),

    # ------------------ Naresh -----------------------

    url(r'^HMS.html/', views.hms_admin_page, name='hms_admin_page'),
    
    url(r'^hms_update/', views.hms_updatation_page, name='hms_updatation_page'), 
    url(r'^hms-admin/', views.form_update, name='form_update'),
    url(r'^hms_middle_update/', views.hms_middle_updatation, name='hms_middle_updatation'),
    url(r'^(?P<id>\d+)/hms_buttom_update/', views.hms_buttom_updatation, name='hms_buttom_updatation'), 
    url(r'^Blog.html/', views.blog_admin_page, name='blog_admin_page'),
    url(r'^(?P<id>\d+)/blog_page_upadete/', views.blog_page_updation, name='blog_page_updation'),
    url(r'^blog-admin/', views.blog_from_update, name='blog_from_update'),
    url(r'^(?P<id>\d+)/bolg_page_delete/', views.bolg_delete, name='bolg_delete'),
    url(r'^add_new_bolg/', views.blog_add_new, name='blog_add_new'),
    url(r'^Home.html/', views.home_index_page, name='home_index_page'),
    url(r'^(?P<id>\d+)/hms_buttom_delete/', views.buttom_delete, name='buttom_delete'),
    url(r'^hms_buttom_add/', views.hms_add_new, name='hms_add_new'),

    url(r'^home-admin/', views.main_update_page, name='main_update_page'),
    url(r'^index_top_update/', views.home_page__top_update, name='home_page__top_update'),
    url(r'^index_mi1_update/', views.home_middle_update, name='home_middle_update'),

    url(r'^index_mi2_update/', views.home_middle2_update, name='home_middle2_update'),

    url(r'^index_mi3_update/', views.home_middle3_update, name='home_middle3_update'),
    url(r'^index_mi4_update/', views.home_middle4_update, name='home_middle4_update'),
    
    url(r'^index_semi_mi_update/', views.home_semi_middle_update, name='home_semi_middle_update'),
    url(r'^index_buttom_update/', views.home_buttom_update, name='home_buttom_update'),
    url(r'^index_add_new/', views.home_add_new, name='home_add_new'),


    url(r'^json_home_hms_blog/', views.home_hms_blog_code, name='home_hms_blog_code'), #this url use for fetch json show_code
    
    # -------------- Sagar -----------------------------------------

    # SERVICES- Header page urls

    url(r'^services-admin', views.services_admin_view, name="services_admin_view"),
    url(r'^services-header-add', views.services_header_add, name="services_header_add"),
    url(r'^(?P<id>\d+)/services-header-update', views.services_header_update, name="services_header_update"),
    url(r'^(?P<id>\d+)/services-header-delete', views.services_header_delete, name="services_header_delete"),
    
    #url(r'^admin-panel', views.admin_panel, name = "admin_panel"),

    url(r'^Services.html/', views.services_view, name="services_view"),

    # SERVICES- Game Plan page urls
    url(r'^game-plan-add', views.services_game_plan_add, name="services_game_plan_add"),
    url(r'^(?P<id>\d+)/game-plan-update', views.services_game_plan_update, name="services_game_plan_update"),
    url(r'^(?P<id>\d+)/game-plan-delete', views.services_game_plan_delete, name="services_game_plan_delete"),
    
    # SERVICES- Features page urls
    url(r'^features-add', views.services_features_add, name="services_features_add"),
    url(r'^(?P<id>\d+)/features-update', views.services_features_update, name="services_features_update"),
    url(r'^(?P<id>\d+)/features-delete', views.services_features_delete, name="services_features_delete"),

    # SERVICES- Our Services page urls
    url(r'^our-services-add', views.services_our_services_add, name="services_our_services_add"),
    url(r'^(?P<id>\d+)/our-services-update', views.services_our_services_update, name="services_our_services_update"),
    url(r'^(?P<id>\d+)/our-services-delete', views.services_our_services_delete, name="services_our_services_delete"),

    # SERVICES- Portfolio page urls
    url(r'^portfolio-add', views.services_portfolio_add, name="services_portfolio_add"),
    url(r'^(?P<id>\d+)/portfolio-update', views.services_portfolio_update, name="services_portfolio_update"),
    url(r'^(?P<id>\d+)/portfolio-delete', views.services_portfolio_delete, name="services_portfolio_delete"),

    url(r'^get_services_data/', views.get_services_api, name="get_services_api"),

     

    url(r'^header-add', views.con_header_add, name="con_header_add"),
    url(r'^(?P<id>\d+)/con-header-update', views.con_header_update, name="con_header_update"),
    url(r'^(?P<id>\d+)/con-header-delete', views.con_header_delete, name="con_header_delete"),
    url(r'contact-us-admin/', views.contact_us_admin_view, name="contact_us_admin_view" ),

    url(r'^get_contactus_data/', views.get_contact_us_api, name="get_contact_us_api"),
    url(r'^Contact-us.html', views.contactus_view, name="contactus_view"),




    url(r'^PortfolioAdmin/', views.PortfolioAdmin, name='PortfolioAdmin'),

    url(r'^web-product-add', views.portfolio_web_product_add, name="portfolio_web_product_add"),
    url(r'^(?P<id>\d+)/web-product-update', views.portfolio_web_product_update, name="portfolio_web_product_update"),
    url(r'^(?P<id>\d+)/web-product-delete', views.portfolio_web_product_delete, name="portfolio_web_product_delete"),

    url(r'^mob-product-add', views.portfolio_mob_product_add, name="portfolio_mob_product_add"),
    url(r'^(?P<id>\d+)/mob-product-update', views.portfolio_mob_product_update, name="portfolio_mob_product_update"),
    url(r'^(?P<id>\d+)/mob-product-delete', views.portfolio_mob_product_delete, name="portfolio_mob_product_delete"),

    url(r'^portfolio-website', views.PortfolioWebsite, name="PortfolioWebsite"),
    url(r'^portfolio-mobile', views.PortfolioMobile, name="PortfolioMobile"),
    url(r'^Portfolio.html', views.portfolio_view, name='portfolio_view'),

 #-------------------Prashant-------------------------
    url(r'^About.html/', views.About_us, name='About_us'),
    
    url(r'^footer/', views.Footer, name='Footer'),
    

    url(r'^aboutus-admin/', views.AbpoutAdmin, name='AbpoutAdmin'),
    url(r'^(?P<id>\d+)/AboutHeaderupdate/', views.About_us_HeaderImg_update, name='About_us_HeaderImg_update'),
    url(r'^(?P<id>\d+)/AboutBodyupdate/', views.About_us_Body_update, name='About_us_Body_update'),
    url(r'^(?P<id>\d+)/AboutBodyImgupdate/', views.About_us_BodyImg_update, name='About_us_BodyImg_update'),
    url(r'^(?P<id>\d+)/AboutImgupdate/', views.About_us_Img_update, name='About_us_Img_update'),
    url(r'^(?P<id>\d+)/Aboutdelete/', views.About_us_Img_Delete, name='About_us_Img_Delete'),
    url(r'^AboutusImgAdd/', views.About_us_Img_Add, name='About_us_Img_Add'),
    url(r'^get_aboutus/', views.get_About_us, name='get_About_us'),


    url(r'^portfolio-admin/', views.PortfolioAdmin, name='PortfolioAdmin'),
    url(r'^get_portfolio/', views.get_portfolio, name='get_portfolio'),
    
    
    
    url(r'^footer-admin/', views.ContactAdmin, name='ContactAdmin'),
    url(r'^(?P<id>\d+)/Contactupdate/', views.ContactUpdate, name='ContactUpdate'),
    url(r'^(?P<id>\d+)/Contactdelete/', views.ContactDelete, name='ContactDelete'),
    url(r'^ContactAdd/', views.ContactAdd, name='ContactAdd'),
    
    
    url(r'^category-add/', views.portfolio_category_add, name='portfolio_category_add'),
    url(r'^(?P<id>\d+)/category-update', views.portfolio_category_update, name='portfolio_category_update'),
    url(r'^(?P<id>\d+)/category-delete', views.portfolio_category_delete, name='portfolio_category_delete'),

    url(r'^article-admin/', views.article_admin, name='article_admin'),
    url(r'^article-add/', views.article_add, name='article_add'),
    url(r'^(?P<id>\d+)/article-update', views.article_update, name='article_update'),
    url(r'^(?P<id>\d+)/article-delete', views.article_delete, name='article_delete'),
    url(r'^Articles.html/', views.article_view, name='article_view'),

    url(r'^copy-right-admin/', views.copy_right_admin, name='copy_right_admin'),
    url(r'^copy-right-add/', views.copy_right_quote_add, name='copy_right_quote_add'),
    url(r'^(?P<id>\d+)/copy-right-update', views.copy_right_quote_update, name='copy_right_quote_update'),
    url(r'^(?P<id>\d+)/copy-right-delete', views.copy_right_quote_delete, name='copy_right_quote_delete'),


    #email url
    url(r'^email/', views.email_send, name='email_send'),
    url(r'^email_sent/', views.email_sent_page, name='email_sent_page'),

    url(r'^admin/', admin.site.urls),

    # url(r'^login/$', views.login, {'template_name': 'login.html', 'authentication_form': LoginForm}),
    # url(r'^logout/$', views.logout, {'next_page': '/login'}), 
]