# -*- coding: utf-8 -*-
# Generated by Django 1.9.11 on 2016-12-06 06:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('amcapp', '0012_servicesportfoliomodel_portfolio_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='PortfolioMobileModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mob_icon', models.ImageField(blank=True, upload_to='images/')),
                ('mob_title', models.CharField(max_length=255)),
                ('mob_image', models.ImageField(blank=True, upload_to='images/')),
            ],
        ),
        migrations.CreateModel(
            name='PortfolioWebsiteModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('web_icon', models.ImageField(blank=True, upload_to='images/')),
                ('web_title', models.CharField(max_length=255)),
                ('web_image', models.ImageField(blank=True, upload_to='images/')),
            ],
        ),
    ]
