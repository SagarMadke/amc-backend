from django.contrib import admin

#importing models 
from .models import ServicesHeaderModel, ServicesGamePlanModel, ServicesFeaturesModel, ServicesOurServicesModel, ServicesPortfolioModel
from .models import ContactUsModel
from .models import PortfolioWebsiteModel, PortfolioMobileModel, PortfolioCategoryModel, ArticleModel, CopyRightModel


#------prashant-----------------

from .models import About_us_HeaderImg
from .models import About_us_Body
from .models import About_us_Img

from .models import About_us_BodyImg
from .models import Portfolio_Page

from .models import Contact_Footernew

# ------------------ Naresh ----------------
from .models import Top_image_icon
from .models import Middle_image_icon
from .models import Buttom_image_icon
from .models import Text_button
from .models import Blog_page
from .models import Home_index_page1
from .models import Home_index_page2
from .models import Home_index_page3
from .models import Home_index_page4
from .models import Home_middle_page1, Home_middle_page2, Home_middle_page3

# -----------Services Page Models----------------
class ServicesHeaderModelAdmin(admin.ModelAdmin):
	list_display = ["id", "header_title", "header_bgimage", ]
	search_fields = ["header_title"]
	class Meta:
		model = ServicesHeaderModel

admin.site.register(ServicesHeaderModel, ServicesHeaderModelAdmin)

class ServicesGamePlanModelAdmin(admin.ModelAdmin):
	list_display = ["id", "plan_title", "plan_image", ]
	search_fields = ["plan_title"]
	class Meta:
		model = ServicesGamePlanModel

admin.site.register(ServicesGamePlanModel, ServicesGamePlanModelAdmin)


class ServicesFeaturesModelAdmin(admin.ModelAdmin):
	list_display = ["id", "features_title", "features_image", ]
	search_fields = ["features_title"]
	class Meta:
		model = ServicesFeaturesModel

admin.site.register(ServicesFeaturesModel, ServicesFeaturesModelAdmin)
	

class ServicesOurServicesModelAdmin(admin.ModelAdmin):
	list_display = ["id", "our_title", "our_image", ]
	search_fields = ["our_title"]
	class Meta:
		model = ServicesOurServicesModel

admin.site.register(ServicesOurServicesModel, ServicesOurServicesModelAdmin)

class ServicesPortfolioModelAdmin(admin.ModelAdmin):
	list_display = ["id", "portfolio_title", "portfolio_image", ]
	search_fields = ["portfolio_title"]
	class Meta:
		model = ServicesPortfolioModel

admin.site.register(ServicesPortfolioModel, ServicesPortfolioModelAdmin)

class ContactUsModelAdmin(admin.ModelAdmin):
	list_display = ["id", "con_title", "con_icon_img"]
	class Meta : 
		model = ContactUsModel

admin.site.register(ContactUsModel, ContactUsModelAdmin)

class PortfolioWebsiteModelAdmin(admin.ModelAdmin):
	list_display = ["id", "web_title", "web_image", "web_category", "web_url",]
	class Meta : 
		model = PortfolioWebsiteModel

admin.site.register(PortfolioWebsiteModel, PortfolioWebsiteModelAdmin)

class PortfolioMobileModelAdmin(admin.ModelAdmin):
	list_display = ["id", "mob_title", "mob_image", "mob_category", "mob_url",]
	class Meta : 
		model = PortfolioMobileModel

admin.site.register(PortfolioMobileModel, PortfolioMobileModelAdmin)

class PortfolioCategoryModelAdmin(admin.ModelAdmin):
	list_display = ["id", "category",]
	class Meta : 
		model = PortfolioCategoryModel

admin.site.register(PortfolioCategoryModel, PortfolioCategoryModelAdmin)

class ArticleModelAdmin(admin.ModelAdmin):
	list_display = ["id", "art_image", "art_title", "art_author",]
	class Meta : 
		model = ArticleModel

admin.site.register(ArticleModel, ArticleModelAdmin)


class CopyRightModelAdmin(admin.ModelAdmin):
	list_display = ["id", "copy_right_quote",]
	class Meta : 
		model = CopyRightModel

admin.site.register(CopyRightModel, CopyRightModelAdmin)


# class ContactAdmin(admin.ModelAdmin):
# 	list_display = ["id", "country"]
# 	class Meta:
# 		model = Contact

# admin.site.register(Contact, ContactAdmin)



# ----------- prashant Admin--------



class About_us_HeaderImg_Admin(admin.ModelAdmin):
	list_display = ["id","Image","Title1", "Title2"]
	class Meta:
		model = About_us_HeaderImg

class About_us_Body_Admin(admin.ModelAdmin):
	list_display = ["id","Icon","Title", "Decription"]
	class Meta:
		model = About_us_Body


class About_us_Img_Admin(admin.ModelAdmin):
	list_display = ["id","Image","Title", "Decription"]
	class Meta:
		model = About_us_Img


class About_us_BodyImg_Admin(admin.ModelAdmin):
	list_display = ["id","Image"]
	class Meta:
		model = About_us_BodyImg


class Portfolio_Page_Admin(admin.ModelAdmin):
	list_display = ["id","Icon","Title", "Image"]
	class Meta:
		model = Portfolio_Page

class Contact_Footer_Admin(admin.ModelAdmin):
	list_display = ["id","country","adress_line1", "adress_line2","state","pin","contact"]
	class Meta:
		model = Contact_Footernew




admin.site.register(About_us_HeaderImg,About_us_HeaderImg_Admin)
admin.site.register(About_us_Body,About_us_Body_Admin)
admin.site.register(About_us_Img,About_us_Img_Admin)
admin.site.register(About_us_BodyImg,About_us_BodyImg_Admin)
admin.site.register(Portfolio_Page,Portfolio_Page_Admin)
admin.site.register(Contact_Footernew,Contact_Footer_Admin)

# ------------- Naresh ---------------------------

admin.site.register(Top_image_icon)
admin.site.register(Middle_image_icon)
admin.site.register(Buttom_image_icon)
admin.site.register(Text_button)
admin.site.register(Blog_page)
admin.site.register(Home_index_page1)
admin.site.register(Home_index_page2)
admin.site.register(Home_index_page3)
admin.site.register(Home_index_page4)

admin.site.register(Home_middle_page1)
admin.site.register(Home_middle_page2)
admin.site.register(Home_middle_page3)