from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm, SetPasswordForm
from django import forms
from .models import ServicesHeaderModel, ServicesGamePlanModel, ServicesFeaturesModel, ServicesOurServicesModel, ServicesPortfolioModel
from .models import ContactUsModel
from .models import PortfolioWebsiteModel, PortfolioMobileModel, PortfolioCategoryModel, ArticleModel, CopyRightModel


from .models import About_us_HeaderImg
from .models import About_us_Body
from .models import About_us_Img
from .models import About_us_BodyImg
from .models import Portfolio_Page
from .models import Contact_Footernew


from .models import Top_image_icon
from .models import Middle_image_icon
from .models import Buttom_image_icon
from .models import Text_button
from .models import Blog_page
from .models import Home_index_page1
from .models import Home_index_page2
from .models import Home_index_page3
from .models import Home_index_page4
from .models import Home_middle_page1, Home_middle_page2, Home_middle_page3




class ServicesHeaderModelForm(forms.ModelForm):
	class Meta:
		model = ServicesHeaderModel
		fields = ["header_title", "header_bgimage", "header_description",]

class ServicesGamePlanModelForm(forms.ModelForm):
	class Meta :
		model = ServicesGamePlanModel
		fields = ["plan_title", "plan_image", "plan_description",]

class ServicesFeaturesModelForm(forms.ModelForm):
	class Meta :
		model = ServicesFeaturesModel
		fields = ["features_title", "features_image", "features_description",]

class ServicesOurServicesModelForm(forms.ModelForm):
	class Meta :
		model = ServicesOurServicesModel
		fields = ["our_title", "our_image", "our_description",]

class ServicesPortfolioModelForm(forms.ModelForm):
	class Meta :
		model = ServicesPortfolioModel
		fields = ["portfolio_title", "portfolio_image", "portfolio_description", "portfolio_url"]


class ContactUsModelForm(forms.ModelForm):
	class Meta :
		model = ContactUsModel
		fields = ["con_title", "con_icon_img", "con_desc1", "con_desc2",]

class PortfolioWebsiteModelForm(forms.ModelForm):
	class Meta:
		model = PortfolioWebsiteModel
		fields = ["web_icon", "web_title", "web_image", "web_category", "web_url",]

class PortfolioMobileModelForm(forms.ModelForm):
	class Meta:
		model = PortfolioMobileModel
		fields = ["mob_icon", "mob_title", "mob_image", "mob_category","mob_url",]

class PortfolioCategoryModelForm(forms.ModelForm):
	class Meta:
		model = PortfolioCategoryModel
		fields = ["category",]

class ArticleModelForm(forms.ModelForm):
	class Meta:
		model = ArticleModel
		fields = ["art_image", "art_date", "art_month", "art_calendar", "art_author","art_title","art_description", ]

class CopyRightModelForm(forms.ModelForm):
	class Meta:
		model = CopyRightModel
		fields = ["copy_right_quote",]


#---------------Prashant-------------------------

class About_us_HeaderImg_Form(forms.ModelForm):
	class Meta:
		model=About_us_HeaderImg
		fields=['Image','Title1','Title2']


class About_us_Body_Form(forms.ModelForm):
	class Meta:
		model=About_us_Body
		fields=['Icon','Title','Decription']

class About_us_Img_Form(forms.ModelForm):
	class Meta:
		model=About_us_Img
		fields=['Image','Title','Decription']

class About_us_BodyImg_Form(forms.ModelForm):
	class Meta:
		model=About_us_BodyImg
		fields=['Image']

class Portfolio_Page_Form(forms.ModelForm):
	class Meta:
		model=Portfolio_Page
		fields=['Icon','Title','Image']


class Contact_Footer_Form(forms.ModelForm):
	class Meta:
		model=Contact_Footernew
		fields=['country','adress_line1','adress_line2','state','pin','contact']

# ----------------------- Naresh-----------------------

class Comment_Form1(forms.ModelForm):
	class Meta :
		model = Top_image_icon
		fields = [
			"image",
			"text_title1",
			"text_title2",
		]

class Comment_Form2(forms.ModelForm):		
	class Meta :
		model = Middle_image_icon
		fields = [
			"icon1",
			"title1",
			"discription",
		]

class Comment_Form3(forms.ModelForm):		
	class Meta :
		model = Buttom_image_icon
		fields = [
			"image_icon",
			"icon2",
			"title2",
			"image_url",
		]

class Comment_Form4(forms.ModelForm):		
	class Meta :
		model = Text_button
		fields = [
			"txt",
		]

class Blog_Form(forms.ModelForm):
	class Meta :
		model = Blog_page
		fields = [
			"blog_image",
			"date_no",
			"date_char",
			"calender",
			#"calender1",
			#"date_char",
			#"timestamp",
			"by",
			"bolg_title",
			"bolg_discription",
		]

class Index_Form1(forms.ModelForm):
	class Meta :
		model = Home_index_page1
		fields = [
			"image_bg1",
			"title_index1",
			"sub_title",
		]

class Index_Form2(forms.ModelForm):
	class Meta :
		model = Home_index_page2
		fields = [
			"icon_image1",
			"title_index2",
			"dis_index1",
		]

class Index_Form_middle(forms.ModelForm):
	class Meta :
		model = Home_middle_page1
		fields = [
			"image_bg2",
			"icon_image2",
			"title_index3",
			"dis_index2",
		]
class Index_Form_middle1(forms.ModelForm):
	class Meta :
		model = Home_middle_page2
		fields = [

			"image_bg2p1",
			"icon_image2p2",
			"title_index3p3",
			"dis_index2p4",
		]
class Index_Form_middle2(forms.ModelForm):
	class Meta :
		model = Home_middle_page3
		fields = [

			"image_bg2p5",
			"icon_image2p6",
			"title_index3p7",
			"dis_index2p8",
		]
class Index_Form3(forms.ModelForm):
	class Meta :
		model = Home_index_page3
		fields = [
			"image_bg3",
			"title_index4",
		]
class Index_Form4(forms.ModelForm):
	class Meta :
		model = Home_index_page4
		fields = [
			"icon_image3",
			"title_index5",
			"dis_index3",
			"image_bottom",
		]

# -------------- User Authentication Forms --------------------------

class LoginForm(AuthenticationForm):
    username = forms.CharField(label="User name", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(label="Password", max_length=30, 
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password'}))


class Password_reset(PasswordResetForm):
    email = forms.EmailField(label="Email Address", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'email'}))
    

class Set_Password(SetPasswordForm):
    password1= forms.CharField(label="Password", max_length=30, 
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password1'}))
    password2= forms.CharField(label="Conform Password", max_length=30, 
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password2'}))
